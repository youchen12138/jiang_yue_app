# 江约APP

## 介绍

发布帖子，寻找陌生人或朋友一起交友

[需求文档入口](./江约需求分析.md)

[接口文档入口](https://www.showdoc.cc/670296803741307)

## 下载

[链接下载](https://www.pgyer.com/jiang_yue)

![二维码扫描下载](https://www.pgyer.com/app/qrcode/jiang_yue)

## Pull Request

请按照以下结构放置代码

```
    jiang_yue_app
    |
    |--frontend--
    |
    |--backend--
```