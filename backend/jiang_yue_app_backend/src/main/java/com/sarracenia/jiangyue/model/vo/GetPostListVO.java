package com.sarracenia.jiangyue.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Sarracenia
 * @date 2020/2/16 22:22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetPostListVO {

    Integer userID;

}
