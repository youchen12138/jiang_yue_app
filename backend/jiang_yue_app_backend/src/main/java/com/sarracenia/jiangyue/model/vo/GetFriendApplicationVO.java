package com.sarracenia.jiangyue.model.vo;

import lombok.Data;

/**
 * @author Sarracenia
 * @date 2020/2/16 15:40
 */
@Data
public class GetFriendApplicationVO {
    Integer userID;
}
