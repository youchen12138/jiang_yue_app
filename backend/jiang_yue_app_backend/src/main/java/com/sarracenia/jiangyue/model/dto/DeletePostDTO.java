package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sarracenia
 * @date 2020/2/13 1:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeletePostDTO {
    Integer buqueding;
}
