package com.sarracenia.jiangyue.dao;

import com.sarracenia.jiangyue.model.entity.User;
import com.sarracenia.jiangyue.model.entity.UserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    long countByExample(UserExample example);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    @Select("select * from user where nickname LIKE CONCAT('%',#{keyWord,jdbcType=VARCHAR},'%') OR     username LIKE CONCAT('%',#{keyWord,jdbcType=VARCHAR},'%')\n")
    List<User> selectByKeyWord(@Param("keyWord")String keyWord);
}