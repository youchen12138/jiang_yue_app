package com.sarracenia.jiangyue.model.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author Sarracenia
 * @date 2020/2/15 22:38
 */
@Data
public class SearchFriendVO {
    @NotNull(message = "内容不能为空")
    @NotEmpty(message = "内容不能为空")
    String addFriendUserName;

    @NotNull
    Integer userID;

}
