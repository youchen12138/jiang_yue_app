package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

import java.util.List;

/**
 * @author Sarracenia
 * @date 2020/2/11 18:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserInfoDTO {
    @Mapping("nickname")
    String nickName;
    String decoration;
    Integer likeNum;
    Integer creditWorthiness;
    @Mapping("avatorurl")
    String avatorURL;
    List<UserArticleInfoDTO> articles;

}
