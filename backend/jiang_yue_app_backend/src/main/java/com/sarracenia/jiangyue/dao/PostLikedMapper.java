package com.sarracenia.jiangyue.dao;

import com.sarracenia.jiangyue.model.entity.PostLiked;
import com.sarracenia.jiangyue.model.entity.PostLikedExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PostLikedMapper {
    long countByExample(PostLikedExample example);

    int deleteByExample(PostLikedExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PostLiked record);

    int insertSelective(PostLiked record);

    List<PostLiked> selectByExample(PostLikedExample example);

    PostLiked selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PostLiked record, @Param("example") PostLikedExample example);

    int updateByExample(@Param("record") PostLiked record, @Param("example") PostLikedExample example);

    int updateByPrimaryKeySelective(PostLiked record);

    int updateByPrimaryKey(PostLiked record);
}