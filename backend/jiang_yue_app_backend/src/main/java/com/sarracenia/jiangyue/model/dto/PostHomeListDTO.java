package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Sarracenia
 * @date 2020/2/16 22:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostHomeListDTO {
    List<PostHomeDTO> data;
}
