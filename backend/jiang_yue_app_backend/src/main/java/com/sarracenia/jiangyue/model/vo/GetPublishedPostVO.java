package com.sarracenia.jiangyue.model.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Sarracenia
 * @date 2020/2/14 22:33
 */
@Data
public class GetPublishedPostVO {
    @NotNull(message = "用户ID不能为空")
    Integer userID;
}
