package com.sarracenia.jiangyue.utils;

import com.sarracenia.jiangyue.model.dto.result.CodeMessage;
import com.sarracenia.jiangyue.exception.GlobalException;
import org.springframework.validation.BindingResult;

/**
 * @author Wei yuyaung
 * @date 2020.01.30 19:54
 */
public class ValidateUtil {

    public static void validateParams(BindingResult results) throws GlobalException {
        if (results.hasErrors()) {
            throw new GlobalException(new CodeMessage(CodeMessage.CODE_PARAM_ERROR, results.getFieldError().getDefaultMessage()));
        }
    }
}
