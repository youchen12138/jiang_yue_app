package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sarracenia
 * @date 2020/2/17 1:12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JoinActivityDTO {
    Integer joinNum;
}
