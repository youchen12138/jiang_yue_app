package com.sarracenia.jiangyue.dao;

import com.sarracenia.jiangyue.model.entity.FocusList;
import com.sarracenia.jiangyue.model.entity.FocusListExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FocusListMapper {
    long countByExample(FocusListExample example);

    int deleteByExample(FocusListExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(FocusList record);

    int insertSelective(FocusList record);

    List<FocusList> selectByExample(FocusListExample example);

    FocusList selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") FocusList record, @Param("example") FocusListExample example);

    int updateByExample(@Param("record") FocusList record, @Param("example") FocusListExample example);

    int updateByPrimaryKeySelective(FocusList record);

    int updateByPrimaryKey(FocusList record);
}