package com.sarracenia.jiangyue.model.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author Sarracenia
 * @date 2020/2/13 0:52
 */
@Data
public class PostInfoVO {
    String title;
    String main;
    String type;
    Date firstTime;
    Date lastTime;
}
