package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Sarracenia
 * @date 2020/2/14 22:35
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostBriefInfoDTO {
    String title;
    String author;
    String content;
    Date lastTime;
    Integer articleID;
}
