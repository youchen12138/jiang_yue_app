package com.sarracenia.jiangyue.model.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Sarracenia
 * @date 2020/2/15 22:40
 */
@Data
public class AddFriendVO {
    @NotNull(message = "userID不能为空")
    Integer userID;
    @NotNull(message = "添加朋友不能为空")
    Integer addFriendID;
}
