package com.sarracenia.jiangyue.dao;

import com.sarracenia.jiangyue.model.entity.ActivityEvaluation;
import com.sarracenia.jiangyue.model.entity.ActivityEvaluationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityEvaluationMapper {
    long countByExample(ActivityEvaluationExample example);

    int deleteByExample(ActivityEvaluationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ActivityEvaluation record);

    int insertSelective(ActivityEvaluation record);

    List<ActivityEvaluation> selectByExample(ActivityEvaluationExample example);

    ActivityEvaluation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ActivityEvaluation record, @Param("example") ActivityEvaluationExample example);

    int updateByExample(@Param("record") ActivityEvaluation record, @Param("example") ActivityEvaluationExample example);

    int updateByPrimaryKeySelective(ActivityEvaluation record);

    int updateByPrimaryKey(ActivityEvaluation record);
}