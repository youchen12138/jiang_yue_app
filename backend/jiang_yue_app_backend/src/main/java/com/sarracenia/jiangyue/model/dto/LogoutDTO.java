package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sarracenia
 * @date 2020/2/14 23:35
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogoutDTO {
    Integer buqueding;

}
