package com.sarracenia.jiangyue.model.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Sarracenia
 * @date 2020/2/17 2:41
 */
@Data
public class UserEvaluateVO {
    @NotNull
    Integer userID;
    @NotNull
    Integer data;
}
