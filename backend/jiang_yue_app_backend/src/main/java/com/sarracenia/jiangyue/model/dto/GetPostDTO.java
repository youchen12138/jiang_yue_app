package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

import java.util.Date;
import java.util.List;

/**
 * @author Sarracenia
 * @date 2020/2/13 0:59
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetPostDTO {

    PostInfoDTO article;


    List<CommentInfoDTO> comment;
}
