package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

import java.util.Date;

/**
 * @author Sarracenia
 * @date 2020/2/11 17:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostHomeDTO {
    @Mapping("postId")
    Integer postID;
    @Mapping("title")
    String postTitle;

    String author;

    String content; // 30字

    @Mapping("tagid")
    Integer tagID;


    Integer commentNum;

    @Mapping("likenum")
    Integer likeNum;

    Date lastTime;


    boolean isLike;


}
