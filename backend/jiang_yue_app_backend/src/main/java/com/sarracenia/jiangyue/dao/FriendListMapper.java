package com.sarracenia.jiangyue.dao;

import com.sarracenia.jiangyue.model.entity.FriendList;
import com.sarracenia.jiangyue.model.entity.FriendListExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FriendListMapper {
    long countByExample(FriendListExample example);

    int deleteByExample(FriendListExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(FriendList record);

    int insertSelective(FriendList record);

    List<FriendList> selectByExample(FriendListExample example);

    FriendList selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") FriendList record, @Param("example") FriendListExample example);

    int updateByExample(@Param("record") FriendList record, @Param("example") FriendListExample example);

    int updateByPrimaryKeySelective(FriendList record);

    int updateByPrimaryKey(FriendList record);
}