package com.sarracenia.jiangyue.dao;

import com.sarracenia.jiangyue.model.entity.UserPost;
import com.sarracenia.jiangyue.model.entity.UserPostExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserPostMapper {
    long countByExample(UserPostExample example);

    int deleteByExample(UserPostExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(UserPost record);

    int insertSelective(UserPost record);

    List<UserPost> selectByExample(UserPostExample example);

    UserPost selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UserPost record, @Param("example") UserPostExample example);

    int updateByExample(@Param("record") UserPost record, @Param("example") UserPostExample example);

    int updateByPrimaryKeySelective(UserPost record);

    int updateByPrimaryKey(UserPost record);
}