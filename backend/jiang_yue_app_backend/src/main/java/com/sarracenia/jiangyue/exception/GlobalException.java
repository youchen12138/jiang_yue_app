package com.sarracenia.jiangyue.exception;


import com.sarracenia.jiangyue.model.dto.result.CodeMessage;
import lombok.Getter;

/**
 * @author sarracenia
 * @date 2020/1/21
 */
@Getter
public class GlobalException extends Exception {

    private static final long serialVersionUID = -3655264493408473309L;

    private CodeMessage codeMessage;

    public GlobalException(CodeMessage codeMessage) {
        super(codeMessage.getMessage());
        this.codeMessage = codeMessage;
    }


}

