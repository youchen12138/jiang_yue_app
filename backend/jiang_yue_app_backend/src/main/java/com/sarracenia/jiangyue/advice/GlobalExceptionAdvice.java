package com.sarracenia.jiangyue.advice;

import com.sarracenia.jiangyue.model.dto.result.BaseResult;
import com.sarracenia.jiangyue.exception.GlobalException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author Sarracenia
 * @date 2020/1/31 15:13
 */
@RestControllerAdvice
public class GlobalExceptionAdvice {

    @ExceptionHandler(value = GlobalException.class)
    public BaseResult<String> handlerAdException(GlobalException globalException) {
        //TODO:怕看漏抛的异常暂且这样写
        globalException.printStackTrace();
        return BaseResult.fail(globalException.getCodeMessage());
    }

}
