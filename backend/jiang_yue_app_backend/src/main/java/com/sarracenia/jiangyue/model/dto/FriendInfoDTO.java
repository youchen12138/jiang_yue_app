package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

/**
 * @author Sarracenia
 * @date 2020/2/15 22:43
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FriendInfoDTO {
    @Mapping("avatorurl")
    String avatorURL;
    String nickname;
    String decoration;
    @Mapping("id")
    Integer userID;
}
