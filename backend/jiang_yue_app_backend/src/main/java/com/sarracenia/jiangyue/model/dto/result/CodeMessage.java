package com.sarracenia.jiangyue.model.dto.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CodeMessage {
    public static final int CODE_SUCCESS = 200;
    /**
     * description: 字段校验错误默认返回1
     * @author: Sarracenia
     * @time: 2020.01.20
     */
    public static final int CODE_PARAM_ERROR = 1;
    public static final CodeMessage VERIFICATION_CODE_ERROR = new CodeMessage(2,"验证码错误");
    public static final CodeMessage USERNAME_DUPLICATE = new CodeMessage(3,"用户名重复");
    public static final CodeMessage ERROR_USERNAME_PASSWORD = new CodeMessage(4,"用户名或密码错误");
    public static final CodeMessage USER_NOT_EXIST = new CodeMessage(5,"该用户不存在");
    public static final CodeMessage ENROLL_FAIL = new CodeMessage(6,"注册失败");
    public static final CodeMessage ERROR_REPASSWORD_PASSWORD = new CodeMessage(7,"请确认输入密码与确认密码是否一致");
    public static final CodeMessage EDIET_USERINFO_FAIL = new CodeMessage(8,"修改个人信息失败");
    public static final CodeMessage POST_NOT_EXIST = new CodeMessage(9,"获取帖子信息失败:帖子不存在");
    public static final CodeMessage LOGOUT_FAIL = new CodeMessage(10,"登出失败");
    public static final CodeMessage FRIEND_APPLICATION_FAIL = new CodeMessage(11,"好友申请失败");
    public static final CodeMessage ERROR_ALREADY_FRIEND = new CodeMessage(12,"已为好友,请勿重复添加好友");
    public static final CodeMessage ERROR_ALREADY_POST_APPLICATION = new CodeMessage(21,"请勿重复发送添加好友请求");
    public static final CodeMessage ERROR_LIKE_STATUS = new CodeMessage(13,"已经不喜欢/喜欢帖子");
    public static final CodeMessage ERROR_ALREADY_JOINED = new CodeMessage(14,"请勿重复加入活动");
    public static final CodeMessage JOIN_ACTIVITY_FAIL = new CodeMessage(15,"加入活动失败");
    public static final CodeMessage ERROR_JOINED_MAX = new CodeMessage(16,"已达参加人数上限");
    public static final CodeMessage ERROR_JOINED_OVERDUE = new CodeMessage(17,"活动已经过期");
    public static final CodeMessage ERROR_ADDFRIEND_ID = new CodeMessage(18,"不可以向自己添加好友");
    public static final CodeMessage ERROR_EVALUATE_NEED_JOIN = new CodeMessage(19,"只允许参加活动成员评价");
    public static final CodeMessage ERROR_ALREADY_EVALUATED = new CodeMessage(20,"请勿重复评价活动");



    public static final CodeMessage INTERNAL_SERVER_ERROR = new CodeMessage(500,"服务器内部错误");
    public static final CodeMessage GENERATE_CAPTCHA_FAIL = new CodeMessage(1001,"生成验证码失败");
    public static final CodeMessage CLOSE_IMAGE_OUTPUT_STREAM_FAIL = new CodeMessage(1002,"关闭图像流失败");

    public static final CodeMessage BLOG_ADD_ERROR = new CodeMessage(2001, "添加博客失败");
    public static final CodeMessage BLOG_DELETE_ERROR = new CodeMessage(2005, "删除博客失败");
    public static final CodeMessage BLOG_EDIT_ERROR = new CodeMessage(2010, "编辑博客失败");
    public static final CodeMessage BLOG_NOT_AUTHENTICATED = new CodeMessage(2015, "没有权限进行此操作");
    public static final CodeMessage BLOG_SEARCH_ERROR = new CodeMessage(2020, "博客搜索失败");
    public static final CodeMessage BLOG_GET_MESSAGE_ERROR = new CodeMessage(2025, "获取博客消息失败");
    public static final CodeMessage BLOG_READ_MESSAGE_ERROR = new CodeMessage(2030, "已读博客消息失败");
    public static final CodeMessage BLOG_GET_MESSAGE_NUM_ERROR = new CodeMessage(2035, "获取博客消息数量失败");
    public static final CodeMessage BLOG_DELETE_COMMENT_ERROR = new CodeMessage(2040, "删除评论失败");
    public static final CodeMessage BLOG_POST_COMMENT_ERROR = new CodeMessage(2045, "发表评论失败");

    public static final CodeMessage BLOG_ARTICLE_NOT_EXIST = new CodeMessage(2011, "操作失败:文章不存在");
    public static final CodeMessage BLOG_ARTICLE_IS_DELETED = new CodeMessage(2012, "操作失败:文章已删除");
    public static final CodeMessage BLOG_ARTICLE_NOT_AUTHOR = new CodeMessage(2013, "操作失败:当前用户非该文章作者");
    public static final CodeMessage BLOG_ARTICLE_AUTHOR_NOT_EXSIT = new CodeMessage(2014, "操作失败:该文章作者不存在");

    public static final CodeMessage BLOG_PAGENUM_ERROR = new CodeMessage(2016, "pageNum错误");

    public static final CodeMessage BLOG_MESSAGE_NOT_EXIST = new CodeMessage(2026, "消息不存在");


    private int code;
    private String message;
}
