package com.sarracenia.jiangyue.model.entity;

import java.util.ArrayList;
import java.util.List;

public class UserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNull() {
            addCriterion("username is null");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNotNull() {
            addCriterion("username is not null");
            return (Criteria) this;
        }

        public Criteria andUsernameEqualTo(String value) {
            addCriterion("username =", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotEqualTo(String value) {
            addCriterion("username <>", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThan(String value) {
            addCriterion("username >", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThanOrEqualTo(String value) {
            addCriterion("username >=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThan(String value) {
            addCriterion("username <", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThanOrEqualTo(String value) {
            addCriterion("username <=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLike(String value) {
            addCriterion("username like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotLike(String value) {
            addCriterion("username not like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameIn(List<String> values) {
            addCriterion("username in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotIn(List<String> values) {
            addCriterion("username not in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameBetween(String value1, String value2) {
            addCriterion("username between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotBetween(String value1, String value2) {
            addCriterion("username not between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("password is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("password is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("password =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("password <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("password >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("password >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("password <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("password <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("password like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("password not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("password in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("password not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("password between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("password not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNull() {
            addCriterion("nickname is null");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNotNull() {
            addCriterion("nickname is not null");
            return (Criteria) this;
        }

        public Criteria andNicknameEqualTo(String value) {
            addCriterion("nickname =", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotEqualTo(String value) {
            addCriterion("nickname <>", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThan(String value) {
            addCriterion("nickname >", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThanOrEqualTo(String value) {
            addCriterion("nickname >=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThan(String value) {
            addCriterion("nickname <", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThanOrEqualTo(String value) {
            addCriterion("nickname <=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLike(String value) {
            addCriterion("nickname like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotLike(String value) {
            addCriterion("nickname not like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameIn(List<String> values) {
            addCriterion("nickname in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotIn(List<String> values) {
            addCriterion("nickname not in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameBetween(String value1, String value2) {
            addCriterion("nickname between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotBetween(String value1, String value2) {
            addCriterion("nickname not between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andAvatorurlIsNull() {
            addCriterion("avatorURL is null");
            return (Criteria) this;
        }

        public Criteria andAvatorurlIsNotNull() {
            addCriterion("avatorURL is not null");
            return (Criteria) this;
        }

        public Criteria andAvatorurlEqualTo(String value) {
            addCriterion("avatorURL =", value, "avatorurl");
            return (Criteria) this;
        }

        public Criteria andAvatorurlNotEqualTo(String value) {
            addCriterion("avatorURL <>", value, "avatorurl");
            return (Criteria) this;
        }

        public Criteria andAvatorurlGreaterThan(String value) {
            addCriterion("avatorURL >", value, "avatorurl");
            return (Criteria) this;
        }

        public Criteria andAvatorurlGreaterThanOrEqualTo(String value) {
            addCriterion("avatorURL >=", value, "avatorurl");
            return (Criteria) this;
        }

        public Criteria andAvatorurlLessThan(String value) {
            addCriterion("avatorURL <", value, "avatorurl");
            return (Criteria) this;
        }

        public Criteria andAvatorurlLessThanOrEqualTo(String value) {
            addCriterion("avatorURL <=", value, "avatorurl");
            return (Criteria) this;
        }

        public Criteria andAvatorurlLike(String value) {
            addCriterion("avatorURL like", value, "avatorurl");
            return (Criteria) this;
        }

        public Criteria andAvatorurlNotLike(String value) {
            addCriterion("avatorURL not like", value, "avatorurl");
            return (Criteria) this;
        }

        public Criteria andAvatorurlIn(List<String> values) {
            addCriterion("avatorURL in", values, "avatorurl");
            return (Criteria) this;
        }

        public Criteria andAvatorurlNotIn(List<String> values) {
            addCriterion("avatorURL not in", values, "avatorurl");
            return (Criteria) this;
        }

        public Criteria andAvatorurlBetween(String value1, String value2) {
            addCriterion("avatorURL between", value1, value2, "avatorurl");
            return (Criteria) this;
        }

        public Criteria andAvatorurlNotBetween(String value1, String value2) {
            addCriterion("avatorURL not between", value1, value2, "avatorurl");
            return (Criteria) this;
        }

        public Criteria andDecorationIsNull() {
            addCriterion("decoration is null");
            return (Criteria) this;
        }

        public Criteria andDecorationIsNotNull() {
            addCriterion("decoration is not null");
            return (Criteria) this;
        }

        public Criteria andDecorationEqualTo(String value) {
            addCriterion("decoration =", value, "decoration");
            return (Criteria) this;
        }

        public Criteria andDecorationNotEqualTo(String value) {
            addCriterion("decoration <>", value, "decoration");
            return (Criteria) this;
        }

        public Criteria andDecorationGreaterThan(String value) {
            addCriterion("decoration >", value, "decoration");
            return (Criteria) this;
        }

        public Criteria andDecorationGreaterThanOrEqualTo(String value) {
            addCriterion("decoration >=", value, "decoration");
            return (Criteria) this;
        }

        public Criteria andDecorationLessThan(String value) {
            addCriterion("decoration <", value, "decoration");
            return (Criteria) this;
        }

        public Criteria andDecorationLessThanOrEqualTo(String value) {
            addCriterion("decoration <=", value, "decoration");
            return (Criteria) this;
        }

        public Criteria andDecorationLike(String value) {
            addCriterion("decoration like", value, "decoration");
            return (Criteria) this;
        }

        public Criteria andDecorationNotLike(String value) {
            addCriterion("decoration not like", value, "decoration");
            return (Criteria) this;
        }

        public Criteria andDecorationIn(List<String> values) {
            addCriterion("decoration in", values, "decoration");
            return (Criteria) this;
        }

        public Criteria andDecorationNotIn(List<String> values) {
            addCriterion("decoration not in", values, "decoration");
            return (Criteria) this;
        }

        public Criteria andDecorationBetween(String value1, String value2) {
            addCriterion("decoration between", value1, value2, "decoration");
            return (Criteria) this;
        }

        public Criteria andDecorationNotBetween(String value1, String value2) {
            addCriterion("decoration not between", value1, value2, "decoration");
            return (Criteria) this;
        }

        public Criteria andIsloginIsNull() {
            addCriterion("isLogin is null");
            return (Criteria) this;
        }

        public Criteria andIsloginIsNotNull() {
            addCriterion("isLogin is not null");
            return (Criteria) this;
        }

        public Criteria andIsloginEqualTo(Boolean value) {
            addCriterion("isLogin =", value, "islogin");
            return (Criteria) this;
        }

        public Criteria andIsloginNotEqualTo(Boolean value) {
            addCriterion("isLogin <>", value, "islogin");
            return (Criteria) this;
        }

        public Criteria andIsloginGreaterThan(Boolean value) {
            addCriterion("isLogin >", value, "islogin");
            return (Criteria) this;
        }

        public Criteria andIsloginGreaterThanOrEqualTo(Boolean value) {
            addCriterion("isLogin >=", value, "islogin");
            return (Criteria) this;
        }

        public Criteria andIsloginLessThan(Boolean value) {
            addCriterion("isLogin <", value, "islogin");
            return (Criteria) this;
        }

        public Criteria andIsloginLessThanOrEqualTo(Boolean value) {
            addCriterion("isLogin <=", value, "islogin");
            return (Criteria) this;
        }

        public Criteria andIsloginIn(List<Boolean> values) {
            addCriterion("isLogin in", values, "islogin");
            return (Criteria) this;
        }

        public Criteria andIsloginNotIn(List<Boolean> values) {
            addCriterion("isLogin not in", values, "islogin");
            return (Criteria) this;
        }

        public Criteria andIsloginBetween(Boolean value1, Boolean value2) {
            addCriterion("isLogin between", value1, value2, "islogin");
            return (Criteria) this;
        }

        public Criteria andIsloginNotBetween(Boolean value1, Boolean value2) {
            addCriterion("isLogin not between", value1, value2, "islogin");
            return (Criteria) this;
        }

        public Criteria andCreditworthinessIsNull() {
            addCriterion("creditWorthiness is null");
            return (Criteria) this;
        }

        public Criteria andCreditworthinessIsNotNull() {
            addCriterion("creditWorthiness is not null");
            return (Criteria) this;
        }

        public Criteria andCreditworthinessEqualTo(Integer value) {
            addCriterion("creditWorthiness =", value, "creditworthiness");
            return (Criteria) this;
        }

        public Criteria andCreditworthinessNotEqualTo(Integer value) {
            addCriterion("creditWorthiness <>", value, "creditworthiness");
            return (Criteria) this;
        }

        public Criteria andCreditworthinessGreaterThan(Integer value) {
            addCriterion("creditWorthiness >", value, "creditworthiness");
            return (Criteria) this;
        }

        public Criteria andCreditworthinessGreaterThanOrEqualTo(Integer value) {
            addCriterion("creditWorthiness >=", value, "creditworthiness");
            return (Criteria) this;
        }

        public Criteria andCreditworthinessLessThan(Integer value) {
            addCriterion("creditWorthiness <", value, "creditworthiness");
            return (Criteria) this;
        }

        public Criteria andCreditworthinessLessThanOrEqualTo(Integer value) {
            addCriterion("creditWorthiness <=", value, "creditworthiness");
            return (Criteria) this;
        }

        public Criteria andCreditworthinessIn(List<Integer> values) {
            addCriterion("creditWorthiness in", values, "creditworthiness");
            return (Criteria) this;
        }

        public Criteria andCreditworthinessNotIn(List<Integer> values) {
            addCriterion("creditWorthiness not in", values, "creditworthiness");
            return (Criteria) this;
        }

        public Criteria andCreditworthinessBetween(Integer value1, Integer value2) {
            addCriterion("creditWorthiness between", value1, value2, "creditworthiness");
            return (Criteria) this;
        }

        public Criteria andCreditworthinessNotBetween(Integer value1, Integer value2) {
            addCriterion("creditWorthiness not between", value1, value2, "creditworthiness");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}