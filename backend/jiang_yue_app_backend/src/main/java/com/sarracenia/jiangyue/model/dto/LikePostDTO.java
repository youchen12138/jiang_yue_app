package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sarracenia
 * @date 2020/2/16 23:05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LikePostDTO {
    boolean isLike;
}
