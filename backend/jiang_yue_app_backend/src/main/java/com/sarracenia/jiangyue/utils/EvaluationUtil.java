package com.sarracenia.jiangyue.utils;

/**
 * @author Sarracenia
 * @date 2020/2/17 2:51
 */
public class EvaluationUtil {
    public static Integer changeCreditWorthiness(Integer level) {
        if (level == 5) {
            return 5;
        } else if (level == 4) {
            return 3;
        } else if (level == 3) {
            return -3;
        } else if (level == 2) {
            return -5;
        } else if (level == 1) {
            return -10;
        } else {
            return 0;
        }

    }
}
