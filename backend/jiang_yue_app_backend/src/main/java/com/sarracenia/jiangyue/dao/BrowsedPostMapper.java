package com.sarracenia.jiangyue.dao;

import com.sarracenia.jiangyue.model.entity.BrowsedPost;
import com.sarracenia.jiangyue.model.entity.BrowsedPostExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BrowsedPostMapper {
    long countByExample(BrowsedPostExample example);

    int deleteByExample(BrowsedPostExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(BrowsedPost record);

    int insertSelective(BrowsedPost record);

    List<BrowsedPost> selectByExample(BrowsedPostExample example);

    BrowsedPost selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") BrowsedPost record, @Param("example") BrowsedPostExample example);

    int updateByExample(@Param("record") BrowsedPost record, @Param("example") BrowsedPostExample example);

    int updateByPrimaryKeySelective(BrowsedPost record);

    int updateByPrimaryKey(BrowsedPost record);
}