package com.sarracenia.jiangyue.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

import java.util.List;

/**
 * @author Sarracenia
 * @date 2020/2/1 18:30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EnrollDTO {

    Integer userid;
}
