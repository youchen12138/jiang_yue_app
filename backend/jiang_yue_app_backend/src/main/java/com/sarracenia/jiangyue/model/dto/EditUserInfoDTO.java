package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

/**
 * @author Sarracenia
 * @date 2020/2/11 18:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditUserInfoDTO {

    String nickname;
    @Mapping("avatorurl")
    String avatorURL;
    String decoration;//简介
}
