package com.sarracenia.jiangyue.model.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Sarracenia
 * @date 2020/2/17 1:10
 */
@Data
public class JoinActivityVO {
    @NotNull
    Integer userID;
    @NotNull
    Integer articleID;
}
