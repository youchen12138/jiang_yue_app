package com.sarracenia.jiangyue.dao;

import com.sarracenia.jiangyue.model.entity.FriendApplication;
import com.sarracenia.jiangyue.model.entity.FriendApplicationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FriendApplicationMapper {
    long countByExample(FriendApplicationExample example);

    int deleteByExample(FriendApplicationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(FriendApplication record);

    int insertSelective(FriendApplication record);

    List<FriendApplication> selectByExample(FriendApplicationExample example);

    FriendApplication selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") FriendApplication record, @Param("example") FriendApplicationExample example);

    int updateByExample(@Param("record") FriendApplication record, @Param("example") FriendApplicationExample example);

    int updateByPrimaryKeySelective(FriendApplication record);

    int updateByPrimaryKey(FriendApplication record);
}