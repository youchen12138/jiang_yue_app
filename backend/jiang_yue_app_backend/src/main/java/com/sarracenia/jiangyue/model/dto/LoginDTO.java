package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author Sarracenia
 * @date 2020/2/11 17:16
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginDTO {

    Integer userID;
    String username;
    String nickname;
    String decoration;
    String avatorURL;
    Integer creditWorthiness;
}
