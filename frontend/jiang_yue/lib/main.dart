//入口
import 'package:flutter/material.dart';
import 'package:jiang_yue/test.dart';
import './app.dart';

import 'package:intro_views_flutter/Models/page_view_model.dart'; // 预加载模块（第三方库）
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:flutter_localizations/flutter_localizations.dart'; // 国际化包

void main() => runApp(BeforeApp());

class BeforeApp extends StatelessWidget {
  final pages = [
    PageViewModel(
        pageColor: const Color(0xFF03A9F4),
        bubble: Image.asset('images/iconRead.png'),
        body: Text(
          '邀请朋友一起进步',
        ),
        title: Text(
          '学习',
        ),
        titleTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
        bodyTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
        mainImage: Image.asset(
          'images/books.png',
          height: 285.0,
          width: 285.0,
          alignment: Alignment.center,
        )),
    PageViewModel(
      pageColor: const Color(0xFF8BC34A),
      iconImageAssetPath: 'images/icon_menu_group.png',
      body: Text(
        '和小伙伴挥洒汗水',
      ),
      title: Text('打球'),
      mainImage: Image.asset(
        'images/playBall.png',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      titleTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
      bodyTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
    ),
    PageViewModel(
      pageColor: const Color(0xFF607D8B),
      iconImageAssetPath: 'images/icon_menu_scan.png',
      body: Text(
        '玩转这个APP，会有更多朋友陪伴着你',
      ),
      title: Text('交友'),
      mainImage: Image.asset(
        'images/friend.png',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      titleTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
      bodyTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // locale: Locale('zh', 'CN'),
      localeResolutionCallback: (Locale locale, Iterable<Locale> supportedLocales) {
        return Locale('zh', 'CN');
      },
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate, // 提供Material组件里本地化的字符串
        GlobalWidgetsLocalizations.delegate, // 定义了小部件默认的文字方向，根据用户使用的语言改变文字的阅读方向
      ],
      supportedLocales: [
        Locale('zh', 'CN'),
        Locale('en', 'US'), // 第一个参数是语言的代码，第二个是地区的代码
      ],
      debugShowCheckedModeBanner: false,
      title: 'IntroViews Flutter',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Builder(
        builder: (context) => IntroViewsFlutter(
          pages,
          showNextButton: true,
          showBackButton: true,
          onTapDoneButton: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MyApp(),
              ),
            );
          },
          pageButtonTextStyles: TextStyle(
            color: Colors.white,
            fontSize: 18.0,
          ),
        ),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'jiangyue',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: App(),
    );
  }
}
