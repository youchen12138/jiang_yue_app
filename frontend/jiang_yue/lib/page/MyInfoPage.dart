import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:jiang_yue/home/Toast.dart';
import 'package:jiang_yue/page/FriendShow.dart';
import 'package:jiang_yue/page/PostListView.dart';
import 'package:jiang_yue/page/UserInfoPage.dart';
import 'package:jiang_yue/page/LoginPage.dart';
import 'package:jiang_yue/personal/ShowMessage.dart';
import 'package:jiang_yue/utils/HttpUtils.dart';
import 'package:jiang_yue/utils/cache/UserCache.dart';
import 'package:jiang_yue/utils/toast/TsUtils.dart';

//我的页面
class MyInfoPage extends StatefulWidget {
  @override
  _MyInfoPageState createState() => _MyInfoPageState();
}

class _MyInfoPageState extends State<MyInfoPage> {
  static const double IMAGE_ICON_WIDTH = 30.0;
  static const double ARROW_ICON_WIDTH = 16.0;


  List titles = ["我的消息", "评论过的帖子", "浏览过的帖子", "已发布的帖子", "参加的活动", "好友列表", "退出登录"];

  TextStyle titleTextStyle = TextStyle(fontSize: 16.0);
  var rightArrowIcon = Image.asset(
    'images/ic_arrow_right.png',
    width: ARROW_ICON_WIDTH,
    height: ARROW_ICON_WIDTH,
  );

  @override
  Widget build(BuildContext context) {
    return initView();
  }

//  构建布局
  Widget initView() {
    if (userInfo.length == 0) {
      setState(() {
        titles = ["我的消息", "评论过的帖子", "浏览过的帖子", "已发布的帖子", "参加的活动", "好友列表"];
      });
    } else {
      setState(() {
        titles = [
          "我的消息",
          "评论过的帖子",
          "浏览过的帖子",
          "已发布的帖子",
          "参加的活动",
          "好友列表",
          "退出登录"
        ];
      });
    }
    return CustomScrollView(reverse: false, shrinkWrap: false, slivers: <
        Widget>[
      new SliverAppBar(
        pinned: false,
        backgroundColor: Colors.green,
        expandedHeight: 180.0,
        iconTheme: new IconThemeData(color: Colors.transparent),
        flexibleSpace: FlexibleSpaceBar(
          collapseMode: CollapseMode.parallax,
          background: new InkWell(
              onTap: () {
                if (userInfo.length == 0) {
                  _login();
                } else {
                  _userDetail();
                }

                //判断是否已登录
              },
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  userInfo.length == 0
                      ? Image.asset(
                          "images/ic_avatar_default.png",
                          width: 60.0,
                          height: 60.0,
                        )
                      : Container(
                          width: 90.0,
                          height: 90.0,
                          margin:
                              const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.transparent,                                                  
                              image: DecorationImage(
                                  image: AssetImage(
                                    userInfo[0].avatorURL,
                                  ),
                                  fit: BoxFit.cover),
                              border:
                                  Border.all(color: Colors.white, width: 2.0)),
                        ),
                  new Container(
                    margin: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                    child: new Text(
                      userInfo.length == 0 ? '点击头像登录' : userInfo[0].nickname,
                      style: new TextStyle(color: Colors.white, fontSize: 16.0),
                    ),
                  ),
                  new Container(
                    margin: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                    child: new Text(
                      userInfo.length == 0
                          ? ''
                          : '信誉积分：' + userInfo[0].creditWorthiness.toString(),
                      style: new TextStyle(color: Colors.white, fontSize: 16.0),
                    ),
                  )
                ],
              )),
        ),
      ),
      new SliverFixedExtentList(
          delegate:
              new SliverChildBuilderDelegate((BuildContext context, int index) {
            String title = titles[index];
            return new Container(
                alignment: Alignment.centerLeft,
                child: new InkWell(
                  onTap: () {
                    if (userInfo.length == 0) {
                      Toast.show(context, '您未登陆，请登录重试');
                      return;
                    }
                    switch (index) {
                      case 0:
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (context) => new ShowMessage()));
                        break;
                      case 1:
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (context) => new PostListView(
                                  type: 'comment',
                                  title: title,
                                )));
                        break;
                      case 2:
                        Navigator.of(context).push(new MaterialPageRoute(  //push => pop
                            builder: (context) => new PostListView(
                                  type: 'browse',
                                  title: title,
                                )));
                        break;
                      case 3:
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (context) => new PostListView(
                                  type: 'publish',
                                  title: title,
                                )));
                        break;
                      case 4:
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (context) => new PostListView(
                                  type: 'activity',
                                  title: title,
                                )));
                        break;
                      case 5:
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (context) => new FriendShow(
                                )));
                        break;
                      case 6:
                        _logout();
                        break;
                      default:
                    }
                  },
                  child: new Column(
                    children: <Widget>[
                      new Padding(
                        padding:
                            const EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
                        child: new Row(
                          children: <Widget>[
                            new Expanded(
                                child: new Text(
                              title,
                              style: titleTextStyle,
                            )),
                            rightArrowIcon,
                          ],
                        ),
                      ),
                      new Divider(
                        height: 1.0,
                      )
                    ],
                  ),
                ));
          }, childCount: titles.length),
          itemExtent: 60.0),
    ]);
  }

  _login() {
    Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
      return new LoginPage(); //路由跳转至登录页
    })).then((data) {
      if (data['success'] == 1) {
        TsUtils.showShort('登录成功');
      }
    });
  }

  _userDetail() {
    Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
      return new UserInfoPage(
        // avatorURL: userInfo[0].avatorURL,
        // nickname: userInfo[0].nickname,
        // decoration: userInfo[0].decoration,
        // username: userInfo[0].username,
      ); //跳转至修改用户信息页
    }));
  }

  _logout() async {
    showDialog(
        builder: (context) => new AlertDialog(
              title: new Text('提示'),
              content: new Text('是否要退出登录'),
              actions: <Widget>[
                new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text('取消')),
                new FlatButton(
                    onPressed: () {
                      postLogout();
                      Navigator.pop(context);
                    },
                    child: new Text('是的'))
              ],
            ),
        context: context);
  }

  postLogout() async {
    var result =
        await HttpUtils.request('http://47.93.201.127:8080/jiangyue/exit', method: HttpUtils.POST, data: {
      "userID": userInfo[0].id,
    });
    if (result['success'] == true) {
      userInfo = [];
      TsUtils.showShort('退出成功');
      setState(() {_MyInfoPageState();});
    }
  }
}
