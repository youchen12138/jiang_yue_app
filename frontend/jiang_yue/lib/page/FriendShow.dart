import 'package:flutter/material.dart';
import 'package:jiang_yue/home/UserShow.dart';
import 'package:jiang_yue/modal/User.dart';
import 'package:jiang_yue/utils/HttpUtils.dart';
import 'package:jiang_yue/utils/cache/UserCache.dart';
import 'package:jiang_yue/utils/cache/cache.dart';
import 'package:jiang_yue/utils/toast/TsUtils.dart';

class FriendShow extends StatefulWidget {
  @override
  _FriendShowState createState() => _FriendShowState();
}

class _FriendShowState extends State<FriendShow> {
  List<Widget> buildGridTileList(List friendList, BuildContext context) {
    List<Widget> widgetList = new List();

    friendList.forEach((friendItem) {
      widgetList.add(
        InkWell(
          onTap: () {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (context) => UserShow(
                      userID: friendItem['userID'],
                    )));
          },
          child: Container(
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              border: new Border.all(width: 1, color: Colors.blue),
            ),
            child: Row(
              children: <Widget>[
                SizedBox(
                  width: 10,
                ),
                Container(
                    height: 80,
                    width: 80,
                    child: CircleAvatar(
                      backgroundImage: AssetImage(
                        friendItem['avatorURL']
                      ),
                    )),
                SizedBox(
                  width: 20,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      friendItem['nickname'],
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Text(
                      friendItem['decoration'],
                      style: TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    });

    return widgetList;
  }

  List cloneFriendList = [];

  _getFriendList() async {
    var result = await HttpUtils.request('http://47.93.201.127:8080/jiangyue/friendList',
        method: HttpUtils.GET, data: {'userID': userInfo[0].id});
    if (result['success'] == true) {
      myFriendList = result['data'];
      if (myFriendList.length != cloneFriendList.length) {
        setState(() {
          cloneFriendList = myFriendList;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    _getFriendList();

    if (cloneFriendList.length == 0) {
      return Scaffold(
        appBar: AppBar(
          title: Text('好友列表'),
        ),
        body: Center(
          child: Text(
            "居然没有好友 d(ŐдŐ๑)",
            style: TextStyle(color: Colors.grey, fontSize: 20, letterSpacing: 10),
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        title: Text('好友列表'),
      ),
      body: GridView.count(
          padding: const EdgeInsets.all(8.0),
          primary: false,
          mainAxisSpacing: 12, // 竖向间距
          crossAxisCount: 2, // 横向 Item 的个数
          crossAxisSpacing: 12.0, // 横向间距
          children: buildGridTileList(myFriendList, context),
          childAspectRatio: 1.5),
    );
  }
}
