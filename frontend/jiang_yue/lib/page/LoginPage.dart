import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jiang_yue/page/RegisterPage.dart';
import 'package:jiang_yue/utils/HttpUtils.dart';
import 'package:jiang_yue/utils/WidgetsUtils.dart';
import 'package:jiang_yue/utils/cache/UserCache.dart';
import 'package:jiang_yue/utils/toast/TsUtils.dart';
import '../modal/User.dart';
import 'MyInfoPage.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  var leftRightPadding = 40.0;
  var topBottomPadding = 4.0;
  var textTips = new TextStyle(fontSize: 16.0, color: Colors.black);
  var hintTips = new TextStyle(fontSize: 15.0, color: Colors.black26);

  var _userPassController = new TextEditingController();
  var _userNameController = new TextEditingController();

  WidgetsUtils widgetsUtils;

  @override
  Widget build(BuildContext context) {
    widgetsUtils = new WidgetsUtils(context);
    return new Scaffold(
        appBar: new AppBar(
          title: widgetsUtils.getAppBar('登录'),
          iconTheme: new IconThemeData(color: Colors.white),
        ),
        body: SingleChildScrollView(
          child: new Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Padding(
                padding: new EdgeInsets.fromLTRB(
                    leftRightPadding, 40.0, leftRightPadding, topBottomPadding),
                child: new TextField(
                  style: hintTips,
                  controller: _userNameController,
                  decoration: new InputDecoration(hintText: "请输入用户名"),
                  obscureText: false,
                ),
              ),
              new Padding(
                padding: new EdgeInsets.fromLTRB(
                    leftRightPadding, 30.0, leftRightPadding, topBottomPadding),
                child: new TextField(
                  style: hintTips,
                  controller: _userPassController,
                  decoration: new InputDecoration(hintText: "请输入用户密码"),
                  obscureText: true,
                ),
              ),
              new InkWell(
                child: new Container(
                    alignment: Alignment.centerRight,
                    child: new Text(
                      '没有账号？马上注册',
                      style: hintTips,
                    ),
                    padding: new EdgeInsets.fromLTRB(
                        leftRightPadding, 10.0, leftRightPadding, 0.0)),
                onTap: (() {
                  Navigator.of(context).push(new MaterialPageRoute(
                      builder: (context) => new RegisterPage()));
                }),
              ),
              new Container(
                width: 360.0,
                margin: new EdgeInsets.fromLTRB(10.0, 40.0, 10.0, 0.0),
                padding: new EdgeInsets.fromLTRB(leftRightPadding,
                    topBottomPadding, leftRightPadding, topBottomPadding),
                child: new Card(
                  color: Color(0xFF63CA6C),
                  elevation: 6.0,
                  child: new FlatButton(
                      onPressed: () {
                        _postLogin(
                            _userNameController.text, _userPassController.text);
                      },
                      child: new Padding(
                        padding: new EdgeInsets.all(10.0),
                        child: new Text(
                          '马上登录',
                          style: new TextStyle(
                              color: Colors.white, fontSize: 16.0),
                        ),
                      )),
                ),
              )
            ],
          ),
        ));
  }

  

  _postLogin(String userName, String userPassword) async {
    var result =
        await HttpUtils.request('http://47.93.201.127:8080/jiangyue/login', method: HttpUtils.POST, data: {
      "username": userName,
      "password": userPassword,
    });
    if (result['success'] == true) {   //通过后端返回的sucess值判断返回我的界面还是留在当前页
      UserInfo user = UserInfo(
        id: result['data']['userID'],
        username: result['data']['username'],
        decoration: result['data']['decoration'],
        nickname: result['data']['nickname'],
        avatorURL: result['data']['avatorURL'],
        creditWorthiness: result['data']['creditWorthiness'],
      );
      userInfo.add(user);
      Navigator.pop(context, {'success': 1});
    } 
    if (result['success'] == false){
      String msg = result['msg'];
      TsUtils.showShort(msg);
    }
  }
}
