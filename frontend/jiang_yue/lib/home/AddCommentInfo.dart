import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:jiang_yue/home/Toast.dart';
import 'package:jiang_yue/modal/Comment.dart';
import 'package:jiang_yue/modal/User.dart';
import 'package:jiang_yue/utils/cache/UserCache.dart';
import 'package:jiang_yue/utils/toast/TsUtils.dart';

class AddCommentInfo extends StatelessWidget {
  TextEditingController addCommentController = TextEditingController(); // 标题控制器
  final articleID;
  final callback;
  AddCommentInfo({
    Key key,
    @required int this.articleID,
    this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            height: 36,
            child: TextField(
              style: TextStyle(color: Colors.white),
              controller: addCommentController,
              cursorColor: Colors.white,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.lightBlue[200],
                contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                isDense: true,
                border: const OutlineInputBorder(
                  gapPadding: 0,
                  borderRadius: const BorderRadius.all(Radius.circular(4)),
                  borderSide: BorderSide(
                    width: 0,
                    style: BorderStyle.none,
                  ),
                ),
              ),
            ),
          ),
        ),
        RaisedButton(
          child: Text('评论'),
          onPressed: () async {
            final content = addCommentController.text;
            if (content.length == 0) {
              Toast.show(context, '输入不能为空');
              return;
            }

            if (userInfo.length == 0) {
              Toast.show(context, '您当前处于未登陆状态，请登录后重试');
              return;
            }

            Response response;
            response = await Dio().post("http://47.93.201.127:8080/jiangyue/addComment", data: {
              "userID": userInfo[0].id,
              "articleID": articleID,
              "content": content
            });

            if (response.data['success']) {
              TsUtils.showShort('评论成功');
              callback();
            } else {
              print('报错： AddCommentInfo（增加评论）');
            }
          },
        )
      ],
    );
  }
}
