import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:jiang_yue/home/PostShow.dart';
import 'package:jiang_yue/modal/User.dart';
import 'package:jiang_yue/utils/HttpUtils.dart';
import 'package:jiang_yue/utils/cache/cache.dart';

class UserShow extends StatelessWidget {
  final int userID;
  UserShow({Key key, this.userID}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: UserShowHome(
        userID: userID,
      ),
    );
  }
}

class UserShowHome extends StatefulWidget {
  final int userID;
  UserShowHome({Key key, this.userID}) : super(key: key);
  @override
  _UserShowHomeState createState() => _UserShowHomeState();
}

class _UserShowHomeState extends State<UserShowHome> {
  Future _handlePostChangeLike(int userID) async {
    Response response = await Dio().get("http://47.93.201.127:8080/jiangyue/userShow", data: {
      "userID": userID,
    });
  }

  Widget buildPosts(List posts) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: posts.length,
      itemBuilder: (BuildContext context, int index) {
        return Stack(
          children: <Widget>[
            Container(
                color: Colors.white,
                padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      posts[index]['title'],
                      style: Theme.of(context).textTheme.title,
                    ),
                    SizedBox(height: 10),
                    Text(posts[index]['author'], style: TextStyle(fontSize: 14)),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      posts[index]['content'],
                      style: Theme.of(context).textTheme.subhead,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(
                      height: 1,
                    ),
                  ],
                )),
            Positioned.fill(
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                    splashColor: Colors.grey.withOpacity(0.3), // 慢慢展开的颜色
                    highlightColor: Colors.grey.withOpacity(0.1), // 高亮颜色
                    onTap: () {
                      int idx = getIndex(posts[index]['articleID']);
                      Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new PostShow(
                                  callback: _handlePostChangeLike,
                                  articleID: posts[index]['articleID'],
                                  index: idx,
                                )),
                      );
                    }),
              ),
            )
          ],
        );
      },
    );
  }

  List userPublishList = [];
  var user;

  handleUserInfo() async {
    var result = await HttpUtils.request('http://47.93.201.127:8080/jiangyue/userShow', method: HttpUtils.GET, data: {
      "userID": widget.userID,
    });

    if (result['success'] == true) {
      userMsg = result['data'];
      userPushList = result['data']['articles'];

      if (userPublishList.length != userPushList.length) {
        setState(() {
          user = userMsg;
          userPublishList = userPushList;
        });
      }

      if (user == null) {
        setState(() {
          user = userMsg;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    handleUserInfo();

    if (user == null) {
      return Center(
        child: Text(
          '没有该好友的信息',
          style: TextStyle(fontSize: 30, letterSpacing: 10, color: Colors.grey),
        ),
      );
    }

    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(children: <Widget>[
                Container(
                    height: 70,
                    width: 70,
                    child: CircleAvatar(
                      backgroundImage: AssetImage(user['avatorURL']),
                      radius: 35,
                    )),
                SizedBox(
                  width: 16,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      user['nickName'],
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    Text(
                      user['decoration'],
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
              ]),
              InkWell(
                onTap: () {},
                child: Container(
                  height: 50,
                  width: 75,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    border: new Border.all(width: 2, color: Colors.green[300]),
                  ),
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: 5,
                      ),
                      Icon(
                        Icons.add,
                        color: Colors.green[300],
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.favorite_border,
                        color: Colors.green[300],
                      ),
                      SizedBox(
                        width: 5,
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
          child: Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text(
                    userPushList.length.toString(),
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    '帖子数',
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              ),
              SizedBox(
                width: 20,
              ),
              Column(
                children: <Widget>[
                  Text(
                    user['likeNum'].toString(),
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    '点赞数',
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              ),
              SizedBox(
                width: 20,
              ),
              Column(
                children: <Widget>[
                  Text(
                    user['creditWorthiness'].toString(),
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    '信誉积分',
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          color: Colors.grey[200],
          height: 20,
        ),
        buildPosts(userPublishList) // 获取用户信息
      ],
    );
  }
}
