import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:jiang_yue/home/Toast.dart';
import 'package:jiang_yue/modal/User.dart';
import 'package:jiang_yue/utils/cache/UserCache.dart';
import 'package:jiang_yue/utils/cache/cache.dart';
import 'package:jiang_yue/utils/toast/TsUtils.dart';

class AddFriendPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('添加朋友'),
      ),
      body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(color: Colors.grey[300]),
          child: AddFriendHome()),
    );
  }
}

class AddFriendHome extends StatefulWidget {
  @override
  _AddFriendHomeState createState() => _AddFriendHomeState();
}

class _AddFriendHomeState extends State<AddFriendHome> {
  String inputValue = '';

  Widget buildTextField() {
    //theme设置局部主题
    return TextField(
      onChanged: (String value) {
        setState(() {
          inputValue = value;
        });
      },
      cursorColor: Colors.black, //设置光标
      decoration: InputDecoration(
          contentPadding: new EdgeInsets.only(left: 0.0),
          border: InputBorder.none,
          icon: Icon(Icons.search),
          hintText: "请输入想添加的朋友昵称",
          hintStyle: new TextStyle(fontSize: 15, color: Colors.black)),
      style: new TextStyle(fontSize: 15, color: Colors.black),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget editView() {
      return Container(
        //修饰黑色背景与圆角
        decoration: new BoxDecoration(
          border: Border.all(color: Colors.black, width: 1.0),
          color: Colors.white,
          borderRadius: new BorderRadius.all(new Radius.circular(15.0)),
        ),
        alignment: Alignment.center,
        height: 36,
        padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
        child: buildTextField(),
      );
    }

    return ListView(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(16),
          child: editView(),
        ),
        FriendListView(
          inputValue: inputValue,
        ),
      ],
    );
  }
}

class FriendListView extends StatelessWidget {
  final String inputValue;
  List friedns = [];

  FriendListView({Key key, this.inputValue}) : super(key: key);

  Future _handleFriend(String value) async {
    if (value.length == 0) {
      friendList = [];
      return;
    }

    Response response = await Dio().get("http://47.93.201.127:8080/jiangyue/showFriends",
        data: {"addFriendUserName": value, "userID": userInfo[0].id});

    friendList = response.data['data'];
  }

  @override
  Widget build(BuildContext context) {
    _handleFriend(inputValue);

    return ListView.builder(
      shrinkWrap: true, //解决无限高度问题
      physics: new NeverScrollableScrollPhysics(), //禁用滑动事件
      itemCount: friendList.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
            ),
            width: 400,
            margin: EdgeInsets.fromLTRB(8, 0, 8, 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: <Widget>[
                        SizedBox(
                          width: 16,
                        ),
                        Container(
                            height: 50,
                            width: 50,
                            child: CircleAvatar(
                              backgroundImage: AssetImage(
                                  friendList[index]['avatorURL'],),
                              radius: 25,
                            )),
                        SizedBox(
                          width: 16,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(friendList[index]['nickname']),
                            Text(friendList[index]['decoration']),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 16,
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    RaisedButton.icon(
                      onPressed: () async {
                        Response response = await Dio().post("http://47.93.201.127:8080/jiangyue/addFriendMessage",
                            data: {"userID": userInfo[0].id, "addFriendID": friendList[index]['userID']});

                        print(response);

                        if (!response.data['success']) {
                          Toast.show(context, response.data['msg']);
                        } else {
                          TsUtils.showShort('发送好友请求成功');
                        }
                      },
                      icon: Icon(Icons.add),
                      label: Text('添加'),
                      elevation: 12,
                      textColor: Theme.of(context).accentColor,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                  ],
                )
              ],
            ));
      },
    );
  }
}
