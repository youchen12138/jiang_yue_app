import 'package:flutter/material.dart';
import 'package:intl/intl.dart'; // 日期第三方包
import 'dart:async';

class SelectTimeHome extends StatefulWidget {
  final callback;

  SelectTimeHome({Key key, this.callback}): super(key: key);

  @override
  _SelectTimeHomeState createState() => _SelectTimeHomeState();
}

class _SelectTimeHomeState extends State<SelectTimeHome> {
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay(hour: 9, minute: 30);

  void _selectDate() async {
    final DateTime date = await showDatePicker(
      context: context,
      initialDate: selectedDate, // 初始日期
      firstDate: DateTime(1900), //
      lastDate: DateTime(2100),
    );

    if (date == null) {
      return;
    }

    widget.callback(date.millisecondsSinceEpoch.toString());

    setState(() {
      selectedDate = date;
    });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _selectDate,
      child: Row(
        children: <Widget>[
          Text(DateFormat.yMd().format(selectedDate)),
          Icon(Icons.arrow_drop_down),
        ],
      ),
    );
  }
}
