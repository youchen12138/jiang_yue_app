import 'package:flutter/material.dart';
import 'package:jiang_yue/modal/Comment.dart';
import 'package:jiang_yue/modal/User.dart';
import 'package:jiang_yue/utils/cache/cache.dart'; // 评论数据模型

class CommentShow extends StatefulWidget {
  @override
  final int articleID;
  CommentShow({
    Key key,
    @required this.articleID,
  }) : super(key: key);

  _CommentShowState createState() => _CommentShowState();
}

class _CommentShowState extends State<CommentShow> {
  Widget buildGrid(List commentInfoList) {
    List<Widget> commentList = []; //先建一个数组用于存放循环生成的widget
    Widget content; //单独一个widget组件，用于返回需要生成的内容widget
    for (var item in commentInfoList) {
      commentList.add(Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 16, top: 16, bottom: 12),
                child: Row(
                  children: <Widget>[
                    Container(
                        height: 50,
                        width: 50,
                        child: CircleAvatar(
                          backgroundImage: AssetImage(
                            item['avatorURL'] // 获取头像
                          ),
                          radius: 25,
                        )),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start, // 次轴对齐
                      children: <Widget>[
                        Text(item['author']),
                        Text(item['decoration'], style: TextStyle(color: Colors.grey))
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          Padding(
            child: Align(
              alignment: Alignment(-0.9, 0),
              child: Text(
                item['content'],
                textAlign: TextAlign.left,
              ),
            ),
            padding: EdgeInsets.only(bottom: 16),
          ),
          Divider()
        ],
      ));
    }
    content = new Column(children: commentList);
    return content;
  }

  @override
  Widget build(BuildContext context) {
    return buildGrid(commentList);
  }
}
