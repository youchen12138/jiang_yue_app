import 'package:flutter/material.dart';

class StarShow extends StatefulWidget {
  final callback;
  final int index;
  StarShow({Key key, this.callback, this.index}) : super(key: key);
  @override
  _StarShowState createState() => _StarShowState();
}

class _StarShowState extends State<StarShow> {
  int starIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 20,
        ),
        SingleChildScrollView(
          child: Row(
            children: <Widget>[
              SizedBox(
                width: 30,
              ),
              GestureDetector(
                child: Icon(
                  Icons.star,
                  color: (starIndex >= 1 ? Colors.yellow : Colors.grey),
                ),
                onTap: () {
                  setState(() {
                    starIndex = 1;
                  });
                  widget.callback(1, widget.index);
                },
              ),
              SizedBox(width: 8,),
              GestureDetector(
                child: Icon(
                  Icons.star,
                  color: (starIndex >= 2 ? Colors.yellow : Colors.grey),
                ),
                onTap: () {
                  setState(() {
                    starIndex = 2;
                  });
                  widget.callback(2, widget.index);
                },
              ),
              SizedBox(width: 8,),
              GestureDetector(
                child: Icon(
                  Icons.star,
                  color: (starIndex >= 3 ? Colors.yellow : Colors.grey),
                ),
                onTap: () {
                  setState(() {
                    starIndex = 3;
                  });
                  widget.callback(3, widget.index);
                },
              ),
              SizedBox(width: 8,),
              GestureDetector(
                child: Icon(
                  Icons.star,
                  color: (starIndex >= 4 ? Colors.yellow : Colors.grey),
                ),
                onTap: () {
                  setState(() {
                    starIndex = 4;
                  });
                  widget.callback(4, widget.index);
                },
              ),
              SizedBox(width: 8,),
              GestureDetector(
                child: Icon(
                  Icons.star,
                  color: (starIndex >= 5 ? Colors.yellow : Colors.grey),
                ),
                onTap: () {
                  setState(() {
                    starIndex = 5;
                  });
                  widget.callback(5, widget.index);
                },
              ),
              SizedBox(width: 8,),
            ],
          ),
        ),
      ],
    );
  }
}
