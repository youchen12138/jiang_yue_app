import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

class TestPage extends StatefulWidget {
  @override
  _TestPageState createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.http),
          onPressed: () async {
            try {
              Response response = await Dio().get("http://47.93.201.127:8080/jiangyue/postList", data: {"userID": 2});
              return print(response.data['data']['data']);
            } catch (e) {
              return print(e);
            }
          },
        ),
        appBar: AppBar(
          title: Text('Sqlite in Flutter'),
        ),
        body: Container());
  }
}
