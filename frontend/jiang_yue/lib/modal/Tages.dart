// 标签数据模型

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Tag {
  Tag({
    @required this.id,
    @required this.icon,
    @required this.content,
  });

  final int id; // 帖子ID
  final Icon icon;
  final Text content;
}

// 转换毫秒数为时间，请使用common_utils库

Tag getTag(int typeIndex){
  Tag tag;
  tags.forEach((tagItem){
    if (tagItem.id==typeIndex) {
      tag=tagItem;
    }
  });
  return tag;
}

List<Tag> tags = [
  Tag(
    id: 0,
    icon: Icon(Icons.directions_run, color: Colors.lightBlue,),
    content: Text('体育', style: TextStyle(
      color: Colors.lightBlue
    ),),
  ),
  Tag(
    id: 1,
    icon: Icon(Icons.storage, color: Colors.lightGreen,),
    content: Text('学习', style: TextStyle(
      color: Colors.lightGreen
    ),),
  ),
  Tag(
    id: 2,
    icon: Icon(Icons.group, color: Colors.yellow[200],),
    content: Text('游玩', style: TextStyle(
      color: Colors.yellow[100]
    ),),
  ),
  Tag(
    id: 3,
    icon: Icon(Icons.device_unknown),
    content: Text('???'),
  ),
];