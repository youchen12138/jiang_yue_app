// 文章类型数据模型

class Type {
  String type;

  Type(int typeIndex){
    switch (typeIndex) {
      case 0:
        this.type='运动';
        break;
      case 1:
        this.type='学习';
        break;
      case 2:
        this.type='聚餐';
        break;
      default:
        this.type='操作错误';
        break;
    }
  }
}