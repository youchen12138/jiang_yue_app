// 评价数据模型

class Appraise {
  Appraise({
    this.id,
    this.grade,
    this.authorID,
    this.articleID,
    this.byAppraise,
  });

  final int id; // 评价ID
  final int grade; // 评价等级
  final int authorID; // 作者ID
  final int articleID; // 所属帖子ID
  final int byAppraise; // 所属帖子ID

}

List<Appraise> appraises = [];