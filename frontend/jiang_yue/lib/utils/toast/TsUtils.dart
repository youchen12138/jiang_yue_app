import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
class TsUtils{
  static showShort(String msg){      //提示信息的弹出
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,    
        backgroundColor: Color(0xff63CA6C),
        textColor: Color(0xffffffff),
    );
  }
}